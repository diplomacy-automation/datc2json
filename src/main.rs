use combine::{attempt, choice, many1, optional, ParseError, Parser, Stream, unexpected_any, value};
use combine::parser::char::{letter, string, string_cmp, space};
use std::iter::FromIterator;
use std::collections::HashMap;

mod provincedb;

fn main() {
    println!("Hello, world!");
}

//case class DatcParser(variant: Variant) extends OrderParser with PhaseParser {
//
//  override protected val whiteSpace: Regex = "( |\\t|#.*)+".r
//
//  def parse(input: String): Either[String, List[Datc]] = parseAll(datc, input) match {
//    case Success(data, next) => Right(data)
//    case NoSuccess(errorMessage, next) => Left(s"$errorMessage on line ${next.pos.line} on column ${next.pos.column}")
//  }
//
//  def datc: Parser[List[Datc]] = rep(LF) ~> header ~> rep(LF) ~> testcases <~ rep(LF)
//
//  def header: Parser[String] = "VARIANT_ALL Standard" <~ LF
//
//  def testcases: Parser[List[Datc]] = repsep(testcase, rep(LF))
//
//  def LF = "\n"
//
//  def testcase: Parser[Datc] = casename ~
//    opt(prestateSetphase) ~ opt(prestateSupplycenterOwners) ~ prestate ~ opt(prestateDislodged) ~ opt(prestateResults) ~
//    orders ~ (poststateBlock | poststateSame) <~ "END" <~ opt(LF) ^^ {
//    case (t ~
//      prePhase ~ owners ~ preSt ~ preD ~ preR ~
//      os ~ postSt) =>
//      val phase = prePhase.getOrElse(Phase(1901, Spring, Movement))
//      val psomap = owners.toList.flatten.foldLeft(Map.empty[Province, Power])((m, up) => m.updated(up.location.province, up.power))
//      val preDis = preD.toList.flatten
//      val preRes = preR.toList.flatten
//      if (postSt._1.isEmpty && postSt._2.isEmpty) {
//        Datc(variant, t, phase, psomap, preSt, preDis, preRes, os, preSt, Seq.empty)
//      } else {
//        Datc(variant, t, phase, psomap, preSt, preDis, preRes, os, postSt._1, postSt._2)
//      }
//  }
//
//  def prestateDislodged: Parser[List[UnitPosition]] = "PRESTATE_DISLODGED" ~> LF ~> rep(state)
//
//  def prestateResults: Parser[List[OrderResult]] = "PRESTATE_RESULTS" ~> LF ~> rep(result)
//
//  def result: Parser[OrderResult] = flag ~ order <~ rep1(LF) ^^ { case (f ~ o) => f(o) }
//
//  def flag: Parser[(Order) => OrderResult] = (success | failure) <~ ":"
//
//  def success: Parser[SuccessResult.type] = "SUCCESS" ^^ { _ => SuccessResult }
//
//  def failure: Parser[(Order) => FailureResult] = "FAILURE" ^^ { _ => FailureResult(_: Order, None) }
//
//  def casename: Parser[String] = "CASE" ~> ".+".r <~ LF ^^ { result => result }
//
//  def prestateSupplycenterOwners: Parser[List[UnitPosition]] = "PRESTATE_SUPPLYCENTER_OWNERS" ~> LF ~> rep(state)
//
//  def prestateSetphase: Parser[Phase] = "PRESTATE_SETPHASE" ~> phase <~ LF
//
//  def prestate: Parser[List[UnitPosition]] = "PRESTATE" ~> LF ~> rep(state)
//
//  def state: Parser[UnitPosition] = power ~ unittype ~ (location <~ rep1(LF)) ^^ { case (p ~ u ~ l) => UnitPosition(p, u, l.setCoast(u)) }
//
//  def orders: Parser[List[Order]] = "ORDERS" ~> rep1(LF) ~> rep(order <~ LF)
//
//  def poststateBlock: Parser[(List[UnitPosition], List[UnitPosition])] = poststate ~ opt(poststateDislodged) ^^ { case (ps ~ pd) => (ps, pd.toList.flatten) }
//
//  def poststate: Parser[List[UnitPosition]] = "POSTSTATE" ~> rep1(LF) ~> rep(state) <~ opt(LF)
//
//  def poststateDislodged: Parser[List[UnitPosition]] = "POSTSTATE_DISLODGED" ~> rep1(LF) ~> rep(state) <~ rep(LF)
//
//  def poststateSame: Parser[(List[UnitPosition], List[UnitPosition])] = "POSTSTATE_SAME" <~ LF ^^ { result => (List.empty[UnitPosition], List.empty[UnitPosition]) }
//}
//
//trait OrderParser extends UnitTypeParser with RegexParsers {
//  def variant: Variant
//
//  val worldMap: WorldMap = variant.worldMap
//
//  def order: Parser[Order] = holdOrder | moveOrder | supportMoveOrder | supportHoldOrder | convoyOrder | buildOrder | removeOrder
//
//  def unitPosition: Parser[UnitPosition] = power ~ unittype ~ location ^^ { case (p ~ u ~ l) => UnitPosition(p, u, l.setCoast(u)) }
//
//  def holdOrder: Parser[HoldOrder] = unitPosition <~ ("(?i)HOLD".r | "H") ^^ { case (u) => HoldOrder(u) }
//
//  def moveOrder: Parser[MoveOrder] = (unitPosition <~ "-") ~ location ~ opt(("via" | "by") ~> "(?i)convoy".r) ^^ {
//    case (up ~ d ~ c) => MoveOrder(up, d.setDstCoast(up.unitType, up.location, worldMap), c.fold(false)(_ => true))
//  }
//
//  def support: Parser[String] = "(?i)SUPPORTS".r | "S"
//
//  def supportHoldOrder: Parser[SupportHoldOrder] = (unitPosition <~ support) ~ unittype ~ location ^^ {
//    case (up ~ ht ~ hs) => SupportHoldOrder(up, ht, hs.setCoast(ht))
//  }
//
//  def supportMoveOrder: Parser[SupportMoveOrder] = (unitPosition <~ support) ~ unittype ~ (location <~ "-") ~ location ^^ {
//    case (up ~ u ~ f ~ to) => SupportMoveOrder(up, u, f, to)
//  }
//
//  def convoyOrder: Parser[ConvoyOrder] = (unitPosition <~ ("(?i)convoys".r | "C" | "c")) ~ unittype ~ (location <~ "-") ~ location ^^ {
//    case (up ~ u ~ f ~ to) => ConvoyOrder(up, u, f, to)
//  }
//
//  def buildOrder: Parser[BuildOrder] = power ~ ("Build" ~> unittype) ~ location ^^ { case (p ~ ut ~ l) => BuildOrder(UnitPosition(p, ut, l.setCoast(ut))) }
//
//  def removeOrder: Parser[RemoveOrder] = power ~ ("Remove" ~> unittype) ~ location ^^ { case (p ~ ut ~ l) => RemoveOrder(UnitPosition(p, ut, l.setCoast(ut))) }
//

fn hold_order<Input>() -> impl Parser<Input, Output=String>
    where Input: Stream<Token=char>,
          Input::Error: ParseError<Input::Token, Input::Range, Input::Position> {
    return (power(), string(":"), space(), unit_type(), space(), location(), space(),
            string("hold").or(string("H")))
        .map(|_| "hold".to_string());
}

fn power<Input>() -> impl Parser<Input, Output=String>
    where Input: Stream<Token=char>,
          Input::Error: ParseError<Input::Token, Input::Range, Input::Position> {
    return many1::<Vec<_>, _, _>(letter()).map(|x| String::from_iter(x));
}

#[derive(PartialEq, Debug)]
struct Location {
    province: String,
    coast: Option<String>,
}

fn location<Input>() -> impl Parser<Input, Output=Location>
    where Input: Stream<Token=char>,
          Input::Error: ParseError<Input::Token, Input::Range, Input::Position> {
    return (province(), optional((string("/"), coast())))
        .map(|(p, c)| Location { province: p, coast: c.map(|x| x.1) });
}

fn province<Input>() -> impl Parser<Input, Output=String>
    where Input: Stream<Token=char>,
          Input::Error: ParseError<Input::Token, Input::Range, Input::Position> {
    return (many1::<Vec<_>, _, _>(letter())
            , optional((space(), many1::<Vec<_>, _, _>(letter()))))
        .map(|(x, y): (Vec<char>, Option<(char, Vec<char>)>)|
            format!("{}{}",
                    String::from_iter(x),
                    y.map_or(String::from(""), |(_, s)| format!(" {}", String::from_iter(s)))));
}

fn province_long<'a, Input: 'a>(db: &'a HashMap<String, String>) -> impl Parser<Input, Output=String> + 'a
    where Input: Stream<Token=char>,
          Input::Error: ParseError<Input::Token, Input::Range, Input::Position> {
//    let x = (many1(letter()), space(), many1(letter())).then(|(f, _, s): (String, _, String)| {
    let x = many1(letter()).then(move |f: String| {
        //        let key = format!("{} {}", f, s);
        let val = db.get(&f);
        return if let Some(cn) = val {
            value(cn).left()
        } else {
            unexpected_any("token").right()
        };
    }).map(|x| x.to_string());
    return x;
}

fn province2<Input>() -> impl Parser<Input, Output=String>
    where Input: Stream<Token=char>,
          Input::Error: ParseError<Input::Token, Input::Range, Input::Position> {
    let x = many1(letter()).then(|s: String|
        (if s == "London" {
            value("hit london").left()
        } else {
            unexpected_any("not london").right()
        })).map(|x| "maybe_london".to_string());
    return x;
}

fn coast<Input>() -> impl Parser<Input, Output=String>
    where Input: Stream<Token=char>,
          Input::Error: ParseError<Input::Token, Input::Range, Input::Position> {
//    return choice(vec!["xc", "nc", "sc", "wc", "ec", "mv"]
//        .into_iter().map( |x| string(x)).collect())
//        .map(|x:&str| x.to_string());
    return choice([string("xc"), string("nc"), string("sc"),
        string("wc"), string("ec"), string("mv")])
        .map(|x| x.to_string());
}

fn army<Input>() -> impl Parser<Input, Output=String>
    where Input: Stream<Token=char>,
          Input::Error: ParseError<Input::Token, Input::Range, Input::Position>
{
    return attempt(string_cmp("army", |l, r| l.eq_ignore_ascii_case(&r)))
        .or(string("a")).or(string("A"))
        .map(|_| "A".to_string());
}

fn fleet<Input>() -> impl Parser<Input, Output=String>
    where Input: Stream<Token=char>,
          Input::Error: ParseError<Input::Token, Input::Range, Input::Position> {
    return attempt(string_cmp("fleet", |l, r| l.eq_ignore_ascii_case(&r)))
        .or(string("f")).or(string("F"))
        .map(|_| "F".to_string());
}

fn unit_type<Input>() -> impl Parser<Input, Output=String>
    where Input: Stream<Token=char>,
          Input::Error: ParseError<Input::Token, Input::Range, Input::Position> {
    return army().or(fleet());
}

#[cfg(test)]
mod test {
    use combine::EasyParser;
    use combine::error::StringStreamError::UnexpectedParse;

    use crate::*;
    use crate::provincedb::read_xml;

    //    #[test]
//    fn test_hold_order() {
//        let mut parser = hold_order();
//        assert_eq!(parser.easy_parse("France: F mao hold"), Ok(("hold".to_string(), "")))
//    }

    #[test]
    fn test_province_long(){
        let db = read_xml();
        let mut parser = province_long(&db);
        assert_eq!(parser.easy_parse("London"), Ok(("lon".to_string(), "")));
        assert_eq!(parser.easy_parse("Berlin"), Ok(("ber".to_string(), "")));
    }
    #[test]
    fn test_province2() {
        let mut parser = province2();
        assert_eq!(parser.easy_parse("London"), Ok(("maybe_london".to_string(), "")));
        assert!(parser.easy_parse("Berlin").is_err());

    }

    #[test]
    fn test_power() {
        let mut parser = power();
        assert_eq!(parser.parse("England"), Ok(("England".to_string(), "")));
    }

    #[test]
    fn test_location() {
        let mut parser = location();
        assert_eq!(parser.parse("London"),
                   Ok((Location { province: "London".to_string(), coast: None }, "")));
        assert_eq!(parser.parse("spain/nc"),
                   Ok((Location { province: "spain".to_string(), coast: Some("nc".to_string()) },
                       "")));
    }

    #[test]
    fn test_province() {
        let mut parser = province();
        assert_eq!(parser.parse("London"), Ok(("London".to_string(), "")));
        assert_eq!(parser.parse("North Sea"), Ok(("North Sea".to_string(), "")));
        assert_eq!(parser.parse("spain/nc"), Ok(("spain".to_string(), "/nc")));
    }

    #[test]
    fn test_coast() {
        let mut parser = coast();
        assert_eq!(parser.parse("sc"), Ok(("sc".to_string(), "")))
    }

    #[test]
    fn test_army() {
        let mut parser = army();
        let result = parser.parse("A");
        assert_eq!(result, Ok(("A".to_string(), "")));

        let result = parser.parse("a");
        assert_eq!(result, Ok(("A".to_string(), "")));

        let result = parser.parse("army");
        assert_eq!(result, Ok(("A".to_string(), "")));

        let result = parser.parse("F");
        assert_eq!(result, Err(UnexpectedParse));
    }

    #[test]
    fn test_fleet() {
        let mut parser = fleet();
        let result = parser.easy_parse("f");
        assert_eq!(result, Ok(("F".to_string(), "")))
    }

    #[test]
    fn test_unit_type() {
        let mut parser = unit_type();
        assert_eq!(parser.parse("A"), Ok(("A".to_string(), "")));
    }
}
